/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.patcharin.usermanagementproject;

import java.util.ArrayList;

/**
 *
 * @author patch
 */
public class UserManagementProject {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "Pass@1234", 'M', 'A');
        User user1 = new User("User1", "User1", "Pass@1234", 'M', 'U');
        User user2 = new User("User2", "User2", "Pass@1234", 'F', 'U');
        System.out.println("admin");
        System.out.println("user1");
        System.out.println("user2");
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("Print for userArr");
        for (int i = 0; i < userArr.length; i++) {
            System.out.println(userArr[i]);
        }
        System.out.println("ArrayList");
        ArrayList<User> userList = new ArrayList<User>();
        userList.add(admin);
        System.out.println(userList.get(userList.size() - 1) + "list size" + userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size() - 1) + "list size" + userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size() - 1) + "list size" + userList.size());
        for(int i=0;i<userList.size();i++){
            System.out.println(userList.get(i));
        }
       
    }
}
